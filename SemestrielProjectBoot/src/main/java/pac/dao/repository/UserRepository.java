package pac.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pac.dao.tables.User;

public interface UserRepository extends JpaRepository<User,Long> {
}
