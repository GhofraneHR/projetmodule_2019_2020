package pac.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pac.dao.tables.Test;

public interface TestRepository extends JpaRepository<Test,Long> {
}
