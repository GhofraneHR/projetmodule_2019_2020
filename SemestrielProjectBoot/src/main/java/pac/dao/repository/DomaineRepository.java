package pac.dao.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import pac.dao.tables.Domaine;

public interface DomaineRepository extends JpaRepository<Domaine,Long>{
}
