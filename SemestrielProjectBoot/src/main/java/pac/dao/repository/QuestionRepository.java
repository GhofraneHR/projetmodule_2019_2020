package pac.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pac.dao.tables.Question;

public interface QuestionRepository extends JpaRepository<Question,Long> {
}
