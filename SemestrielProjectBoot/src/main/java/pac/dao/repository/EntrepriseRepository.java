package pac.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pac.dao.tables.Entreprise;

public interface EntrepriseRepository extends JpaRepository<Entreprise,Long> {
}
