package pac.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pac.dao.tables.Member;

public interface MemberRepository extends JpaRepository<Member,Long> {
}
