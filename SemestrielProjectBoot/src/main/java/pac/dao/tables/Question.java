package pac.dao.tables;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Question {
    @Id
    private Long id;
    private String questionText;
    private List<String>responceList;
    private String rightAnsewer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public List<String> getResponceList() {
        return responceList;
    }

    public void setResponceList(List<String> responceList) {
        this.responceList = responceList;
    }

    public String getRightAnsewer() {
        return rightAnsewer;
    }

    public void setRightAnsewer(String rightAnsewer) {
        this.rightAnsewer = rightAnsewer;
    }
}
