package pac.dao.tables;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Document {
    @Id
    private Long id;
    private Date creationDate;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
