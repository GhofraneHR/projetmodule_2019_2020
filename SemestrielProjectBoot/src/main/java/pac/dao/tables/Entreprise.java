package pac.dao.tables;

import javax.persistence.Entity;
import java.util.List;

@Entity
public class Entreprise extends User {
    private List<Interet> interets;
    private String adresse;

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }


    public List<Interet> getInterets() {
        return interets;
    }

    public void setInterets(List<Interet> interets) {
        this.interets = interets;
    }
}
