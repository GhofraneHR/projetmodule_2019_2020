package pac.dao.tables;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Degree {
    @Id
    private Long idMember;
    @Id
    private Long idTest;
    private float degree;

    public Long getIdMember() {
        return idMember;
    }

    public void setIdMember(Long idMember) {
        this.idMember = idMember;
    }

    public Long getIdTest() {
        return idTest;
    }

    public void setIdTest(Long idTest) {
        this.idTest = idTest;
    }

    public float getDegree() {
        return degree;
    }

    public void setDegree(float degree) {
        this.degree = degree;
    }
}
