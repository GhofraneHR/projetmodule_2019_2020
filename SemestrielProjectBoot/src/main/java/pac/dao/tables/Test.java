package pac.dao.tables;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Test {
    @Id
    private Long id;
    private String name;
    private List<Question> questions;


    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
