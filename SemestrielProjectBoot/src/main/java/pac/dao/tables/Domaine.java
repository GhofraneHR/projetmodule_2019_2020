package pac.dao.tables;


import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Domaine {

    @Id
  private Long id;
    private String name;
   private List<Interet> interets;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Interet> getInterets() {
        return interets;
    }

    public void setInterets(List<Interet> interets) {
        this.interets = interets;
    }
}
