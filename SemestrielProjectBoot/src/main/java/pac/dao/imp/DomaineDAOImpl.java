package pac.dao.imp;


import org.springframework.stereotype.Repository;
import pac.dao.DomaineDAO;
import pac.dao.tables.Domaine;
import pac.dao.repository.DomaineRepository;

import java.util.List;

@Repository
public class DomaineDAOImpl implements DomaineDAO {
    private final DomaineRepository domaineRepository;

    public DomaineDAOImpl(DomaineRepository domaineRepository) {
        this.domaineRepository = domaineRepository;
    }

    @Override
    public void addDomaine(Domaine domaine) {
        domaineRepository.save(domaine);
    }

    @Override
    public List<Domaine> getAllDomaines() {
        List A = domaineRepository.findAll();
        return A;

    }
}
