package pac.dao;

import pac.dao.tables.Domaine;

import java.util.List;

public interface DomaineDAO {
    void addDomaine(Domaine domaine);
    List<Domaine> getAllDomaines();
}
