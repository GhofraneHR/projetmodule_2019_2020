package pac.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pac.dao.tables.Domaine;
import pac.service.DomaineService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/api/domaine")
public class DomaineController {
    @Autowired
    private DomaineService domaineService;

    @PostMapping(value = "/add")
    public String addAgent(@RequestBody Domaine domaine) {
        domaineService.addDomaine(domaine);
        return "Domaine created.";
    }
    @GetMapping("/all")
    public List<Domaine> getAllDomaines() {
        return domaineService.getAllDomaines();

    }

}
