package pac.service;

import pac.dao.tables.Domaine;

import java.util.List;

public interface DomaineService {
    void addDomaine(Domaine domaine);
    List<Domaine> getAllDomaines();

}
