package pac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pac.dao.DomaineDAO;
import pac.dao.tables.Domaine;
import pac.service.DomaineService;

import java.util.List;

@Service
@Transactional
public class DomaineServiceImpl implements DomaineService {
    private final DomaineDAO domaineDAO;

    @Autowired
    public DomaineServiceImpl(DomaineDAO domaineDAO) {
        this.domaineDAO = domaineDAO;
    }

    @Override
    public void addDomaine(Domaine domaine) {
        domaineDAO.addDomaine(domaine);
    }

    @Override
    public List<Domaine> getAllDomaines() {
        return domaineDAO.getAllDomaines();
    }
}
